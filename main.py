from fastapi import FastAPI

from app.controller.todo_controller import todo_router
from app.controller.user_controller import user_router

app = FastAPI()

app.router.prefix = "/api/v1"

app.include_router(user_router, prefix="/users")
app.include_router(todo_router, prefix="/todos")