from pydantic import BaseModel, EmailStr
from app.schemas.todo_schema import TodoSchema
from typing import List

class UserBase(BaseModel):
    email: EmailStr

class UserSchema(UserBase):
    name: str
    
    class Config:
        orm_mode = True

class UserCreateSchema(UserSchema):
    password: str

    class Config:
        orm_mode = False

class UserFullSchema(UserSchema):
    todos: List[TodoSchema]