from pydantic import BaseModel

class TodoBaseSchema(BaseModel):
    text: str
    completed: bool

class TodoSchema(TodoBaseSchema):
    owner_id: str

    class Config:
        orm_mode = True

class TodoResponseSchema(TodoSchema):
    id: str

class TodoUpdateSchema(TodoBaseSchema):
    id: str