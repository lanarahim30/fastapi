from curses.ascii import US
from typing import List

from sqlalchemy.orm import Session

from app.models.todo_model import TODO
from app.models.user_model import User
from app.schemas.todo_schema import TodoSchema, TodoUpdateSchema


def add_todo(
        db: Session,
        current_user: User,
        todo_data: TodoSchema,
):
    todo: User = User(
        text=todo_data.text,
        completed=todo_data.completed,
    )
    todo.owner = current_user
    db.add(todo)
    db.commit()
    db.refresh(todo)
    return todo


def update_todo(
        db: Session,
        new_todo: TodoUpdateSchema,
):
    todo: User = db.query(User).filter(
        User.id == new_todo.id,
    ).first()
    todo.text = new_todo.text
    todo.completed = new_todo.completed
    db.commit()
    db.refresh(todo)
    return todo


def delete_todo(
        db: Session,
        todo_id: int,
):
    todo: TODO = db.query(TODO).filter(TODO.id == todo_id).first()
    db.delete(todo)
    db.commit()

def get_user_todos(
        db: Session,
        current_user: User,
) -> List[TODO]:
    todos = db.query(TODO).filter(TODO.owner == current_user).all()
    return todos