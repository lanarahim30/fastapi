from typing import List

from fastapi import APIRouter
from fastapi.params import Depends
from sqlalchemy.orm import Session

from app.services import todo_service
from app.services.user_service import get_current_user
from app.config.database import get_db
from app.models.user_model import User
from app.schemas.todo_schema import TodoSchema, TodoBaseSchema, TodoUpdateSchema, TodoResponseSchema

todo_router = APIRouter()

@todo_router.get('', response_model=List[TodoResponseSchema])
def get_my_todos_view(db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    todos = todo_service.get_user_todos(db, current_user)
    return todos


@todo_router.post('', response_model=List[TodoResponseSchema])
def add_todo_view(
        todo_data: TodoBaseSchema,
        db: Session = Depends(get_db),
        current_user: User = Depends(get_current_user),
):
    todo_service.add_todo(
        db,
        current_user,
        todo_data,
    )
    todos = todo_service.get_user_todos(db, current_user)
    return todos


@todo_router.put('', response_model=List[TodoResponseSchema])
def update_todo_view(
        todo_data: TodoUpdateSchema,
        db: Session = Depends(get_db),
        current_user: User = Depends(get_current_user),
):
    todo_service.update_todo(
        db,
        new_todo=todo_data,
    )
    todos = todo_service.get_user_todos(db, current_user)
    return todos


@todo_router.delete('/{todo_id:int}', response_model=List[TodoResponseSchema])
def delete_todo_view(
        todo_id: int,
        db: Session = Depends(get_db),
        current_user: User = Depends(get_current_user)
):
    todo_service.delete_todo(db, todo_id)
    todos = todo_service.get_user_todos(db, current_user)
    return todos