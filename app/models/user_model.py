from app.config.database import Base
from sqlalchemy import Column , String, Text, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

class User(Base):
    __tablename__ = 'users'
    id = Column(UUID(as_uuid=True), nullable=False, primary_key=True)
    name = Column(String(255), nullable=False)
    email = Column(String(100), unique=True, nullable=False)
    password: Column(Text, nullable=True)
    time_created = Column(DateTime(timezone=True), server_default=func.now())
    time_updated = Column(DateTime(timezone=True), onupdate=func.now())
    todos = relationship('TODO', back_populates='owner', cascade="all, delete-orphan")
